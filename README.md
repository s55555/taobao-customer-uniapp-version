## 项目简介

九朵云淘客APP分为开源版本和商业版本 此项目是开源免费版本， 此项目基于uniapp开发，集成了京东联盟,多多进宝,淘宝,抖好货,美团,饿了么 会员特权卡等

* 基于uniapp vue开发的一款淘宝客APP， 前端模板，完整版
* ####前端模板全部开源 后端java编写,如有需要,有偿提供
* 后端采用java语言编写,Spring Boot、Spring Cloud & Alibaba。
* 具体功能可下载APP体验https://m3w.cn/hws
* 该程序仅开放前端 后端测试服务器 学生机  比较差 请各位大佬手下留情。
*  ####此项目经历了无数个通宵才开发完成，请大家务必好评 ，定制，需要私有化部署的联系QQ 212156620    或微信 jiuge8631 
*  uniapp qq技术交流群 458190194 有啥学习遇到的问题可以群里找大家交流
* 代理版本APP开发中,功能表看这里http://yycdn.osjava.cn/QQ%E5%9B%BE%E7%89%8720210109162944.jpg
* 您的支持就是我最大的动力
* ####软件订制开发、产品合作、技术合作、创业合作等可以加我微信 jiuge8631 

* ####开源版本去掉了短信注册功能,因不可抗力因素,去掉了分享商品海报接口(商品分享如果是本地商品库,可正常分享海报,如果是联盟商品库,分享失败,如果你是淘客开发者,联系作者获取分享接口)

### **特别声明:该源码仅供学习使用,请遵守开源协议，即便是在国内！禁止将修改后和衍生的代码做为闭源的商业软件发布和销售，产生的一切后果与本人无关，如需商用可联系作者提供商业化服务**



#### 友情链接 [九朵云淘客](https://www.jiuduoyun.net/) 



## 功能表

<img src="https://cdn.java3.cn/uploads/20210109/lr4zOrRpj8mbuYJ3zmVI9JhW1ime.jpg"/>




## APP演示图

<table>
    <tr>
        <td><img src="https://cdn.java3.cn/uploads/20210109/Fvnjq_SrCB3Qh4FkjeZtQosm3DMk.jpg"/></td>
        <td><img src="https://cdn.java3.cn/uploads/20210109/FtUm4beZqHuS91vkR32kgE0a_IXq.jpg"/></td>
    </tr>
    <tr>
        <td><img src="https://cdn.java3.cn/uploads/20210109/FrxJzmRsAg6Zr-vtiTEik-WCDs6o.jpg"/></td>
        <td><img src="https://cdn.java3.cn/uploads/20210109/FvSm-IXmXpnHjYBr0h1oBQanM4Mh.jpg"/></td>
    </tr>
    <tr>
        <td><img src="https://cdn.java3.cn/uploads/20210109/Ft_bjAK7-1LNrYpaq1E77dPocb_C.jpg"/></td>
        <td><img src="https://cdn.java3.cn/uploads/20210109/FlmV7Ox7gqC_Q7Lc8GLhoptp3DNR.jpg"/></td>
    </tr>
	<tr>
        <td><img src="https://cdn.java3.cn/uploads/20210109/FtqXJMNMRrw_0vyUSqC93iA5ik-h.jpg"/></td>
        <td><img src="https://cdn.java3.cn/uploads/20210109/Ft0jeT5YxlVRcR8CozmUltkZx3hz.jpg"/></td>
    </tr>	 
 
</table>


## 后台截图

<table>

   <tr>
        <td><img src="https://cdn.java3.cn/uploads/20210109/FjVhzPNRe3XaPo-XzZFOBKcnKr7a.png"/></td>
        <td><img src="https://cdn.java3.cn/uploads/20210109/Fji82nbjvxIV6JjRS0ztpF3InIvo.png"/></td>
    </tr>



</table>


## 代理APP抢先看

<table>
 <td><img src="https://cdn.java3.cn/uploads/20210109/Fn_yj0u4u11r5Ss-d0QN4j8mvxQ0.jpg"/></td>
</table>



代理APP6折预售中。。

## 开源版使用须知

1.允许用于个人学习、毕业设计、教学案例、公益事业;

2.如果商用必须保留版权信息，请自觉遵守;

3.禁止将本项目的代码和资源进行任何形式的出售，产生的一切任何后果责任由侵权者自负。

3.请遵守开源协议,即便是在国内




## 特别鸣谢
* ColorUI-UniApp https://ext.dcloud.net.cn/plugin?id=239    
* 热更新  https://ext.dcloud.net.cn/plugin?id=1643
* uview  https://ext.dcloud.net.cn/plugin?id=1593
* 等等,如果还有未写到的,联系作者添加
 



## uniapp交流群

QQ群： [![加入QQ群](https://img.shields.io/badge/458190194-blue.svg)](https://jq.qq.com/?_wv=1027&k=LMEuFZja) 点击按钮入群。


## 捐赠代理版的研发

如果您认为此插件帮到了您的开发工作，使您赚到钱了,您可以捐赠九朵云淘客的研发工作，捐赠无门槛，哪怕是一杯可乐也好(相信这比打赏主播更有意义)。

<table>

   <tr>
        <td><img src="https://cdn.java3.cn/uploads/20210109/Fgptc1HnM2TmW-h5z6oetT-KI112.png"/></td>
        <td><img src="https://cdn.java3.cn/uploads/20210109/Fie1LUseO3n66CTCNeJm8FRL7GDk.png"/></td>
    </tr>


</table>